﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGM_Task27.Models
{
    // Magdeli Holmøy Asplin
    // 9/4/2019

    // This is the supervisor class which can be used to create supervisor objects. They have an id, a name and a number of 
    // years of experience
    public class Supervisor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int YearsOfExperience { get; set; }
    }
}
