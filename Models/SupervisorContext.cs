﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGM_Task27.Models
{
    // Magdeli Holmøy Asplin
    // 9/4/2019

    // This is the database class for the supervisor class
    public class SupervisorContext : DbContext
    {
        public SupervisorContext(DbContextOptions<SupervisorContext> options) : base(options)
        {

        }

        public DbSet<Supervisor> Supervisors { get; set; }
    }
}
