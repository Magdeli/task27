﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PGM_Task27.Models;

namespace PGM_Task27
{
    // Magdeli Holmøy Asplin
    // 9/4/2019

    // This is the home controller file for this web API 


    [Route("api/supervisor")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetSupervisor()
        {
            return _context.Supervisors;
        }
    }
}